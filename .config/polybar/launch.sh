#!/bin/bash

# Kill all running instances of polybar
killall -q polybar

# Wait for polybar to quit
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if [[ $(xrandr | grep "HDMI-0 connected") ]]; then
    polybar -r -c $HOME/.config/polybar/config.ini primary &
    polybar -r -c $HOME/.config/polybar/config.ini secondary &
else
    # Monitor name can be one of these
    if [[ $(xrandr | grep "eDP-1-1") ]]; then
        polybar -r -c $HOME/.config/polybar/config.ini single-nvidia &
    else
        polybar -r -c $HOME/.config/polybar/config.ini single &
    fi
fi

echo "Polybar launched..."

