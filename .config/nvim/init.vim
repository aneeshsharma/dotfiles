set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'neoclide/coc.nvim', {'branch': 'release'}

Plugin 'tpope/vim-fugitive'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'preservim/nerdtree'
Plugin 'ryanoasis/vim-devicons'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'kaicataldo/material.vim', { 'branch': 'main' }
Plugin 'rakr/vim-one'
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'morhetz/gruvbox'
Plugin 'rust-lang/rust.vim'
Plugin 'iamcco/markdown-preview.nvim'

Plugin 'ghifarit53/tokyonight-vim'
Plugin 'dracula/vim', {'name': 'dracula'}

call vundle#end()            " required
filetype plugin indent on    " required

let g:rustfmt_autosave = 1

let g:coc_global_extensions = [
    \'coc-pairs',
    \'coc-eslint',
    \'coc-prettier',
    \'coc-json',
    \'coc-pyright',
    \'coc-tsserver',
    \'coc-clangd',
    \'coc-yaml',
    \'coc-sh',
    \'coc-html',
    \'coc-css',
    \'coc-rust-analyzer'
    \]

if exists('+termguicolors')
  let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

let g:material_theme_style = 'darker'
let g:material_terminal_italics = 1

let g:tokyonight_style = "night"
let g:tokyonight_italic_functions = 1
colorscheme material

let g:airline_theme='wombat'

hi Normal guibg=None ctermbg=None
set cursorline

" Keybindings
source ~/.config/nvim/keymap.vim

" Open NERDTreee when vim starts
autocmd VimEnter * NERDTree | wincmd p

" Set NERDTree directory if vim is opened with a directory
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif

" Open NERDTree in every tab
autocmd BufWinEnter * silent NERDTreeMirror

" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif

" Prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile

" Delete trailing spaces
command! -nargs=0 RmTrail :%s/\s\+$//e

" Delete trailing spaces on save
autocmd BufWritePre *.py RmTrail

" Some defaults
set smartindent
set noerrorbells
set nowrap
set nobackup
set noswapfile

set undodir=~/.vim/undodir
set undofile
set incsearch

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set number relativenumber
syntax on
set autoindent
set clipboard+=unnamedplus

set colorcolumn=80

set mouse=a

let g:neovide_refresh_rate=120
let g:neovide_transparency=1.0

