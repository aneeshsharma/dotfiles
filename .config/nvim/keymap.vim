let mapleader = " "

" Window management
nnoremap <silent> <leader>h :wincmd h<CR>
nnoremap <silent> <leader>j :wincmd j<CR>
nnoremap <silent> <leader>k :wincmd k<CR>
nnoremap <silent> <leader>l :wincmd l<CR>

nnoremap <silent> <leader>H :wincmd H<CR>
nnoremap <silent> <leader>J :wincmd J<CR>
nnoremap <silent> <leader>K :wincmd K<CR>
nnoremap <silent> <leader>L :wincmd L<CR>

nnoremap <silent> <Leader>> :vertical resize +5<CR>
nnoremap <silent> <Leader>< :vertical resize -5<CR>

nnoremap <silent> <Leader>+ :res +5<CR>
nnoremap <silent> <Leader>- :res -5<CR>

nnoremap <silent> <Leader>s :split<CR>
nnoremap <silent> <Leader>v :vs<CR>

" Start terminal in this window
nnoremap <Leader>t :ter<CR>

" Open terminal window
nnoremap <silent> <Leader>` :split<CR> :wincmd j<CR> :ter<CR> :res 15<CR> :echo "Opened Terminal"<CR> 

" NERDTree configs
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <C-t> :NERDTreeRefreshRoot<CR>

" Fuzzy Finder
nnoremap <C-p> :FZF<CR>

" Exit terminal input
tnoremap <Esc> <C-\><C-n>

" Prettier
nnoremap <leader>f :Prettier<CR> :w<CR>

