#!/usr/bin/zsh

nm-applet &
blueman-applet &
optimus-manager-qt &
nitrogen --restore &
flameshot &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
conky -p 5 &
bash -c "sleep 20 && kdeconnect-indicator" &

# Only if on AC power start these
if ! [[ $(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep discharging) ]]; then
    discord &
fi

