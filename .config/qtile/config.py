#     _          _     _     _
#    / \   _ __ (_)___| |__ ( )___
#   / _ \ | '_ \| / __| '_ \|// __|
#  / ___ \| | | | \__ \ | | | \__ \
# /_/   \_\_| |_|_|___/_| |_| |___/
#   ___ _____ _ _
#  / _ \_   _(_) | ___
# | | | || | | | |/ _ \
# | |_| || | | | |  __/
#  \__\_\|_| |_|_|\___|
#                   __ _
#   ___ ___  _ __  / _(_) __ _
#  / __/ _ \| '_ \| |_| |/ _` |
# | (_| (_) | | | |  _| | (_| |
#  \___\___/|_| |_|_| |_|\__, |
#                        |___/
# Copyright (c) 2021 Anish Sharma

import re
import os
import time
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile import qtile
from libqtile.config import Click, Drag, Group, Key, Screen, Match
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

BACKGROUND = '000a1a'

HIGHLIGHT = BACKGROUND
BUTTON = BACKGROUND

LAYOUT = '002a2a'
DISABLED = '535353'

COLOR_5 = BACKGROUND
COLOR_4 = BACKGROUND
COLOR_3 = BACKGROUND
COLOR_2 = BACKGROUND
COLOR_1 = BACKGROUND

BORDER = '0050ff'

HOME = os.getenv("HOME")

launcher = f'{HOME}/.config/rofi/bin/launcher_colorful'
powermenu = f'{HOME}/.config/rofi/bin/powermenu'

mod = "mod4"
terminal = guess_terminal()

keys = [
    Key([mod], "n", lazy.to_screen(1), desc="Switch to screen 2"),
    Key([mod], "m", lazy.to_screen(0), desc="Switch to screen 1"),
    # Switch between windows in current stack pane
    Key([mod], "j", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "k", lazy.layout.up(),
        desc="Move focus up in stack pane"),
    Key([mod], "h", lazy.layout.left(),
        desc="Move focus left in stack pane"),
    Key([mod], "l", lazy.layout.right(),
        desc="Move focus right in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "control"], "j", lazy.layout.shuffle_down(),
        desc="Move window down in current stack "),
    Key([mod, "control"], "k", lazy.layout.shuffle_up(),
        desc="Move window up in current stack "),
    Key([mod, "control"], "h", lazy.layout.shuffle_left(),
        desc="Move window left in current stack "),
    Key([mod, "control"], "l", lazy.layout.shuffle_right(),
        desc="Move window down in current stack "),

    # Resize windows
    Key([mod, "shift"], "l", lazy.layout.grow(), lazy.layout.increase_nmaster(),
        desc="Grow window"),
    Key([mod, "shift"], "h", lazy.layout.shrink(), lazy.layout.decrease_nmaster(),
        desc="Shrink"),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    # Media keys
    Key([], 'XF86AudioRaiseVolume', lazy.spawn(
        'pactl set-sink-volume @DEFAULT_SINK@ +4%')),
    Key([], 'XF86AudioLowerVolume', lazy.spawn(
        'pactl set-sink-volume @DEFAULT_SINK@ -4%')),
    Key([], 'XF86AudioMute', lazy.spawn(
        'pactl set-sink-mute @DEFAULT_SINK@ toggle')),
    Key([], 'XF86AudioPlay', lazy.spawn('playerctl play-pause')),
    Key([], 'XF86AudioNext', lazy.spawn('playerctl next')),
    Key([], 'XF86AudioPrev', lazy.spawn('playerctl previous')),

    # Applications
    Key([mod], "b", lazy.spawn('brave'), desc="Start Brave"),
    Key([mod], "v", lazy.spawn('pavucontrol'), desc="Volume Control"),
    Key([mod], "t", lazy.spawn('thunar'), desc="Start File Manager"),

    # Screenshot
    Key([mod], "s", lazy.spawn('flameshot gui'),
        desc="Take Screenshot with Flameshot"),

    Key([], 'XF86MonBrightnessDown', lazy.spawn('xbacklight -dec 10')),
    Key([], 'XF86MonBrightnessUp', lazy.spawn('xbacklight -inc 10')),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod, "shift"], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
    Key([mod], 'r', lazy.spawn(launcher)),
    Key([mod], 'x', lazy.spawn(powermenu))
]


class ContainsMatch:
    def __init__(self, value):
        self.value = value

    def __call__(self, window):
        data = window.cmd_inspect()
        if data['name']:
            if re.search(self.value, data['name'], re.I):
                return True
        if data['wm_class']:
            if self.value in data['wm_class']:
                return True
        return False


def get_matches(strings):
    return [Match(func=ContainsMatch(s)) for s in strings]

def init_group_names():
    return [
        # Web
        ("\uf269", {'layout': 'max',
                    'matches': get_matches(["Firefox", "brave-browser"])}, '1'),
        # Dev
        ("\uf489", {'layout': 'monadtall'}, '2'),
        # Chat
        ("\uf865", {'layout': 'stack', 'matches': get_matches(
            ["Discord", "discord", "Ferdi", "ferdi"])}, '3'),
        # System
        ("\uf85a", {'layout': 'monadtall'}, '4'),
        # Music
        ("\ufc58", {'layout': 'stack', 'matches': get_matches(
            ["Spotify", "spotify"])}, '8'),
        # File manager
        ("\uf7f4", {'layout': 'monadtall', 'matches': get_matches(
            ["Thunar", "Doplhin", "dolphin", "thunar"])}, '9'),
        # Video
        ("\ufa7b", {'layout': 'max', 'matches': get_matches(["vlc"])}, '0'),
        # Games
        ("\uf11b", {'layout': 'floating'}, 'minus')
    ]


def init_groups():
    return [Group(name, **kwargs) for name, kwargs, key_map in group_names]


if __name__ in ['config', '__main__']:
    group_names = init_group_names()
    groups = init_groups()

for i, (name, kwargs, key_map) in enumerate(group_names, 0):
    keys.extend([
        Key([mod], str(key_map), lazy.group[name].toscreen(),
            desc="Switch to group {}".format(name)),
        Key([mod, "shift"], str(key_map), lazy.window.togroup(name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(name)),
    ])

layouts = [
    layout.Max(),
    layout.Stack(num_stacks=1, border_width=0),
    layout.Matrix(border_focus=BORDER),
    layout.MonadTall(margin=8, border_focus=BORDER, border_width=3),
    layout.Floating(border_focus=BORDER)
]

widget_defaults = dict(
    font='JetBrainsMono Nerd Font',
    style='Bold',
    fontsize=14,
    padding=3,
)
extension_defaults = widget_defaults.copy()


def get_song_data():
    # Get meta data of current player
    lines = os.popen('playerctl metadata').read().split('\n')

    # Get status of current player
    status = os.popen('playerctl status').read().strip()

    # Set a char for representing status
    status_char = '\uf885'

    if status == 'Playing':
        status_char = '\uf04b'
    elif status == 'Paused':
        status_char = '\uf04c'

    data = {}
    for line in lines:
        i = line.find(':')
        if i == -1:
            continue
        line = line[i + 1:]
        i_s = line.find(' ')
        if i_s == -1:
            continue
        name = line[:i_s]
        value = line[i_s:].strip()
        data[name] = value
    output = []

    try:
        if data["title"]:
            output.append(data["title"])
    except KeyError:
        pass

    try:
        if data["album"]:
            output.append(data["album"])
    except KeyError:
        pass
    try:
        if data["artist"]:
            output.append(data["artist"])
    except KeyError:
        pass

    output = ' | '.join(output)

    step_time = 1
    length = 30

    l = len(output)

    if l == 0:
        return status_char.strip()

    if l <= length:
        return status_char + ' '*2 + output

    output = output + ' ' * 5
    l = len(output)
    start = int(time.time() / step_time) % l
    end = min(start+length, l)

    end_2 = length - (end - start)

    return status_char + ' '*2 + output[start:end] + output[0:end_2]


def toggle_player():
    return qtile.cmd_spawn('playerctl play-pause')

def start_power_menu():
    return qtile.cmd_spawn(powermenu)

playerDataWidget = widget.GenPollText(func=get_song_data,
                                      update_interval=5,
                                      background=COLOR_3,
                                      padding=8,
                                      foreground="ffafaf",
                                      markup=False,
                                      mouse_callbacks={'Button1': toggle_player})

primary_bar = bar.Bar(
    [
        widget.CurrentLayoutIcon(scale=0.5, background=LAYOUT),
        widget.GroupBox(highlight_method='line',
                        this_current_screen_border="ffff00",
                        highlight_color=[HIGHLIGHT, HIGHLIGHT],
                        background=BUTTON,
                        inactive=DISABLED,
                        margin=0,
                        margin_y=3,
                        padding=16,
                        spacing=0,
                        fontsize=16),
        widget.Sep(padding=16, linewidth=0),
        widget.Prompt(prompt="\ue602\ue602 "),
        widget.WindowName(),
        widget.Chord(
            chords_colors={
                'launch': ("#ff0000", "#ffffff"),
            },
            name_transform=lambda name: name.upper(),
        ),
        widget.Sep(padding=16, linewidth=0),
        widget.Systray(),
        widget.Sep(padding=8, linewidth=0),
        widget.Volume(fmt='\ufa7d {}',
                      padding=8, background=COLOR_2, foreground="ff5f5f"),
        widget.CPU(
            format='\uf85a {freq_current}GHz {load_percent}%', foreground="ffff00", padding=8, update_interval=5, background=COLOR_3),
        widget.Memory(format='\uf0c9 {MemUsed: .0f}M', padding=8, update_interval=5, background=COLOR_3, foreground="90ff90"),
        widget.Net(format='\ufbf1 {down} {up}', padding=8, background=COLOR_3, foreground="bfafff", update_interval=5),
        widget.Clock(format='\uf133  %a, %d %b %I:%M %p', background=COLOR_4,
                      padding=16, update_interval=30, foreground='ffaf00'),
        widget.Battery(charge_char='\uF583', discharge_char='\uF581',
                       empty_char='\uF579', full_char='\uF578', format="{char} {percent:2.0%}", padding=8, background=COLOR_5, show_short_text=False),
        widget.TextBox(text=' \uF011 ', fontsize=18, foreground='ff9090', padding=0, background=BACKGROUND, mouse_callbacks={'Button1': start_power_menu} ),
        widget.Sep(padding=16, linewidth=0, background=BACKGROUND),
    ],
    32,
    background=BACKGROUND
)

secondary_bar = bar.Bar(
    [
        widget.CurrentLayoutIcon(scale=0.5, background=LAYOUT),
        widget.GroupBox(highlight_method='line',
                        this_current_screen_border="ffff00",
                        highlight_color=[HIGHLIGHT, HIGHLIGHT],
                        background=BUTTON,
                        inactive=DISABLED,
                        margin=0,
                        margin_y=3,
                        padding=16,
                        spacing=0,
                        fontsize=20),
        widget.Sep(padding=16, linewidth=0),
        widget.Prompt(prompt="\ue602\ue602 "),
        widget.WindowName(),
        widget.Chord(
            chords_colors={
                'launch': ("#ff0000", "#ffffff"),
            },
            name_transform=lambda name: name.upper(),
        ),
        playerDataWidget,
        widget.Clock(format='\uf133  %a, %d %b %I:%M %p', background=COLOR_4,
                      padding=16, update_interval=30, foreground='ffaf00'),
        widget.Battery(charge_char='\uF583', discharge_char='\uF581',
                       empty_char='\uF579', full_char='\uF578', format="{char} {percent:2.0%}", padding=8, background=COLOR_5, show_short_text=False),
        widget.TextBox(text=' \uF011 ', fontsize=18, foreground='ff9090', padding=0, background=BACKGROUND, mouse_callbacks={'Button1': start_power_menu} ),
        widget.Sep(padding=16, linewidth=0, background=BACKGROUND),
    ],
    32,
    background=BACKGROUND
)

screens = [
    Screen(
        top=primary_bar,
    ),
    Screen(
        top=secondary_bar,
    )
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

floating_roles = [
    'GtkFileChooserDialog',
    'Dialog',
]

floating_classes = [
    'blueman-applet',
    'blueman-manager',
    'gcr-prompter',
    'confirm',
    'dialog',
    'download',
    'error',
    'file_progress',
    'notification',
    'splash',
    'toolbar',
    'confirmreset',
    'makebranch',
    'ssh-askpass',
    'ocs-url',
    'pavucontrol'
]

floating_types = [
    'dialog',
]

floating_names = [
    'Steam',
]


class FloatingMatch:
    def __call__(self, window):
        data = window.cmd_inspect()
        # open('/home/anish/crash.txt', 'w').write(pprint.pformat(data))
        role = data.get('wm_window_role', None)
        cl = data.get('wm_class', None)
        w_type = data.get('wm_type', None)
        w_name = data.get('wm_name', None)

        if w_name in floating_names:
            return True

        if role in floating_roles:
            return True
        for item in cl:
            if item in floating_classes:
                return True
        if w_type in floating_types:
            return True
        return False


dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = True
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    Match(func=FloatingMatch()),
])

auto_fullscreen = True
focus_on_window_activation = "smart"


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.run([home + '/.config/autostart.sh'])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
