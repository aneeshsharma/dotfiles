#
# ~/.bashrc
#

# If not running interactively, don't do anything

# Fix java application issues
export _JAVA_AWT_WM_NONREPARENTING=1

[[ $- != *i* ]] && return

session=bspwm

export DESKTOP_SESSION=$session

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export TERM=alacritty
alias ssh='TERM=xterm-256color ssh'

# Startx automatically when logging in
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
	exec startx ~/.xinitrc $session &> /dev/null
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Custom functions
source ~/.bash_functions

export PYENV_ROOT=$HOME/.pyenv
export PYENV_SHIMS=$HOME/.pyenv/shims

export PATH=$PATH:~/.local/bin:$PYENV_ROOT:$PYENV_SHIMS

export PIPENV_VENV_IN_PROJECT="enabled"

export DOTFILES="dotfiles"

# rofi launchers
export PATH=$HOME/.config/rofi/bin:$PATH

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/extras/CUPTI/lib64

alias config='/usr/bin/git --git-dir=$HOME/$DOTFILES --work-tree=$HOME'

# gem executables
export PATH=$PATH:$HOME/.local/share/gem/ruby/3.0.0/bin

export GOPATH=$HOME/go

# go executables
export PATH=$PATH:$GOPATH/bin

source "$HOME/.cargo/env"
