#!/usr/bin/zsh

# Get graphics mode
prime-offload
graphics_mode=$(optimus-manager --print-mode)

# Only run picom if running with nvidia graphics
# Bluring too heavy for intel graphics
if [[ $graphics_mode == *"nvidia"* ]]; then
    picom -b &   
fi

