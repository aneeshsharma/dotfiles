#!/bin/zsh

if [[ $(xrandr | grep "HDMI-0 connected") ]]; then
    xrandr --output HDMI-0 --mode 1920x1080 --rate 120 --primary
    xrandr --output eDP-1-1 --mode 1920x1080 --noprimary --left-of HDMI-0
    nvidia-settings --assign CurrentMetaMode="DPY-0: 1920x1080_120 @1920x1080 +1920+0 {ViewPortIn=1920x1080, ViewPortOut=1920x1080+0+0, ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"
    xrandr --output HDMI-0 --mode 1920x1080 --rate 120 --primary
    export DUAL_SCREEN=true
else
    # Monitor name can be one of these
    if [[ $(xrandr | grep "eDP-1-1") ]]; then
        xrandr --output eDP-1-1 --mode 1920x1080 --primary
    else
        xrandr --output eDP-1 --mode 1920x1080 --primary
    fi

    export DUAL_SCREEN=false
fi
