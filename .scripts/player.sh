#!/bin/sh
# Script to prioritize spotify as the player for playerctl commands
spotify_status=$(playerctl --player=spotify status)

if [[ $spotify_status ]]; then
    playerctl --player=spotify $1
else
    playerctl $1
fi
