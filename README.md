# Anish's Dotfiles

A few highlights
- Using Zsh with [OhMyZsh](https://ohmyz.sh/)
    - Using [Powerlevel10k](https://github.com/romkatv/powerlevel10k) prompt
- [QTile](http://www.qtile.org/) as the window manager
- [Rofi](https://github.com/davatorium/rofi) as run launcher
- [Jonaburg Picom](https://github.com/jonaburg/picom) as compositor
- [BSPWM](https://github.com/baskerville/bspwm) as window manager
- [Polybar](https://github.com/polybar/polybar) as bar for BSPWM

I have an Nvidia GPU so a few of the configurations are setup according to that.

### Rofi Theme

I am using a rofi theme collection located [here](https://github.com/adi1090x/rofi)

### Spotify

Using the spotify player script from [here](https://github.com/Jvanrhijn/polybar-spotify) to display spotify status on polybar

## Screen Situation

I have a laptop which I often dock to an external monitor screen. So, the `.scripts/screen.sh`
is supposed to setup the second screen with the correct resolution and refresh
rate if it is connected. It also sets up a few parameters using `nvidia-settings`
to avoid screen tearing due to different refresh rates of the 2 screens.

Also, since it is a laptop, I only start the nvidia GPU if I am plugged in and use
the Intel GPU when the laptop is booted up on battery. So, picom is only started
if I am running on nvidia GPU as Intel Integrated Graphics cannot handle the blurring
effects. Plus not running picom also saves on battery.

