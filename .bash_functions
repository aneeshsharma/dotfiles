#!/usr/bin/bash

alias screen-refresh='~/.config/qtile/screen.sh'
alias update='yay -Syu'

alias la='ls -A'
alias ..='cd ..'
alias gs='git status'

alias vim='nvim'
alias vi='nvim'

# My 3.5mm audio jack port creates weird pop noises when I plug in any audio device
# to it. This solution was found on stackoverflow and works
# I have no idea why it works or how it works
# Yes I can have it run on every login or something but its handy to have it as a function
function fix-pop-sound() {
    sudo hda-verb /dev/snd/hwC0D0 0x20 SET_COEF_INDEX 0x67
    sudo hda-verb /dev/snd/hwC0D0 0x20 SET_PROC_COEF 0x3000
}

function remap-mouse() {
    # The grep part tells which device to select. For example if the name starts with "Gaming Mouse  " (the spaces are intentional).
    # Use xinput list to list your input devices and see which device you need to remap.
    GAMING_MOUSE_ID=$(xinput list | grep -G "Gaming Mouse  " | head -n 1 | sed -r 's/.*id=([0-9]+).*/\1/')

    # Now use xinput to remap the mouse buttons to the new numbers as below
    # The sequence below removes 8 9, replacing with 10 11
    # This removes the back and forward behaviour for the extra mouse buttons found on some devices
    xinput --set-button-map ${GAMING_MOUSE_ID} 1 2 3 4 5 6 7 10 11
}
